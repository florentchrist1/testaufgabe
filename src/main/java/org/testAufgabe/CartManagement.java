package org.testAufgabe;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


import javax.lang.model.element.Element;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class CartManagement {
    public final String[] selectedArticled;

    public WebDriver driver;

    public CartManagement() {
        super();
        this.selectedArticled = new String[]{};
    }

    public WebDriver open() {
        System.setProperty("webdriver.chrome.driver", "drivers/chromedriver.exe");

        WebDriver driver = new ChromeDriver();

        driver.get("https://www.amazon.com/");

        return driver;
    }

    public void addProductToCart(String amazonUrl, String amazonQuery , String url) {
        WebDriver driver = this.open();
        System.out.print("Verifying initial cart contents...");
        String cartElement = "";
        try {
            cartElement = driver.findElement(By.id("nav-cart-count")).getText();
        } catch (org.openqa.selenium.NoSuchElementException e) {
            // Couldn't find cart count, try alternate lookup.

            System.err.println("ERROR");
            System.err.println("Unable to find cart count.");
            System.exit(1);
        }
        int cartCount = Integer.parseInt(cartElement);
        if (cartCount == 0) {
            System.out.println("PASS");
        } else {
            System.err.println("FAIL");
            System.err.println("Initial cart count was nonzero: " + cartCount);
            System.exit(1);
        }

        System.out.print("Performing search...");
        WebElement searchBox = driver.findElement(By.id("twotabsearchtextbox"));

        searchBox.sendKeys(amazonQuery + Keys.RETURN);
        driver.manage().timeouts().pageLoadTimeout(10, TimeUnit.SECONDS);
        if ( driver.getCurrentUrl().compareTo(amazonUrl) != 0) {
            /* Page successfully changed... don't know if it's the right page but going to assume it is. */
            System.out.println("PASS");
        } else {
            System.err.println("FAIL");
            System.err.println("Page did not change.");
            System.exit(2);
        }

        System.out.print("Adding item to cart...");

        driver.get(url);
       driver.manage().timeouts().pageLoadTimeout(20, TimeUnit.SECONDS);



        WebElement basket = driver.findElement(By.xpath("//*[@id='add-to-cart-button']"));

        basket.click();


        System.out.println("PASS");

        System.out.print("Verifying final cart contents...");
        cartElement = driver.findElement(By.id("nav-cart-count")).getText();
        cartCount = Integer.parseInt(cartElement);
        if (cartCount == 1) {
            System.out.println("PASS");
        } else {
            System.err.println("FAIL");
            System.err.println("Final cart count was not one: " + cartCount);
            System.exit(3);
        }

        System.out.println("All tests passed.");

    }

    public void checkPrice( String url ){

        WebDriver driver = this.open();

        driver.get(url);
     List<WebElement> ryzen75800xs = driver.findElements(By.xpath("//span[@class='a-price-whole']"));
   for( int i = 0; i < ryzen75800xs.size();i++){

       if(i == 0) {
           String text = ryzen75800xs.get(0).getText();
           try{
               int number = Integer.parseInt(text);
               if(number == 241){
                   System.out.println("Test sucessfully");
               }

           }
           catch (NumberFormatException ex){
               ex.printStackTrace();

                  }

           }
       }

    }

}

